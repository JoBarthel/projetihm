# BrazHero

# How to build ? 

Clone this project and open it into Unity3D. 

# How to play ? 

Keyboard : QD or arrow keys to move. Space, up, or Z to jump.
Controller : Use a joystick or the d-pad to move. Any button to jump.

You can play the game at http://lemarcha.iiens.net/brazhero.php