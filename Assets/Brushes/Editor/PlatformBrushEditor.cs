﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

[CustomEditor(typeof(PlatformBrush))]
public class PlatformBrushEditor : GridBrushEditorBase
{
    private PlatformBrush _brush
    {
        get { return target as PlatformBrush; }
    }

    public override void OnPaintInspectorGUI()
    {
        GUILayout.Label(_brush.CurrentRotation.ToString());
        _brush.currentSprite = (Sprite) EditorGUILayout.ObjectField("Sprite" , _brush.currentSprite, typeof(Sprite), true);
        

    }

    public override void OnPaintSceneGUI(GridLayout gridLayout, GameObject brushTarget, BoundsInt position, GridBrushBase.Tool tool, bool executing)
    {
        base.OnPaintSceneGUI(gridLayout, brushTarget, position, tool, executing);
        if (Event.current.character.Equals('(') && Event.current.type == EventType.KeyDown)
        {
            _brush.RotateTile();
        }
    }
}

#endif