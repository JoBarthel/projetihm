﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Tilemaps;
using UnityEngine;

[CustomEditor(typeof(DoorBrush))]
public class DoorBrushEditor : GridBrushEditorBase
{
    
    protected DoorBrush _brush { get => target as DoorBrush;}
    public override void OnPaintInspectorGUI()
    {
        base.OnPaintInspectorGUI();
        if (GUILayout.Button("Reset"))
        {
            _brush.ResetState();
        }
    }
}

#endif