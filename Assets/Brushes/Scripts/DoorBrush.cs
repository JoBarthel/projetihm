﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(order = 2, menuName = "Brushes/DoorBrush", fileName = "DoorBrush")]
[CustomGridBrush(false, true, false, "Prefab Brush / Platform Brush")]
public class DoorBrush : UnityEditor.Tilemaps.GridBrush
{
    [SerializeField] private GameObject doorPrefab;
    [SerializeField] private GameObject brasierPrefab;
    [SerializeField] private int zPos;
    private DoorBrushState state = DoorBrushState.Door;
    private Door previousDoor;
 
    public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        Vector3Int cellPosition = new Vector3Int(position.x, position.y, zPos);
        
        if(GetObjectInCell(gridLayout, brushTarget.transform, new Vector3Int(position.x, position.y, zPos)) != null)
            return;
        
        if(state == DoorBrushState.Door)
            SpawnDoor(gridLayout, brushTarget, cellPosition);
        else if (state == DoorBrushState.Brasier)
            SpawnBrasier(gridLayout, brushTarget, cellPosition);
    }

    private void SpawnBrasier(GridLayout gridLayout, GameObject brushTarget, Vector3Int cellPosition)
    {
        GameObject instance = PrefabUtility.InstantiatePrefab(brasierPrefab.gameObject, brushTarget.transform) as GameObject;
        Vector3 centerTile = new Vector3(.5f, .5f, 0f);
        instance.transform.position =
            gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(cellPosition + centerTile));
        var brasier = instance.GetComponent<Brasero>();
        brasier.SetFireLineSpeedAndRotation(previousDoor.transform.position);
        brasier.ControlledDoor = previousDoor;
        state = DoorBrushState.Door;
        
    }

    private void SpawnDoor(GridLayout gridLayout, GameObject brushTarget, Vector3Int cellPosition)
    {
        GameObject instance = PrefabUtility.InstantiatePrefab(doorPrefab, brushTarget.transform) as GameObject;
        Vector3 centerTile = new Vector3(.5f, .0f, 0f);
        instance.transform.position =
            gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(cellPosition + centerTile));
        previousDoor = instance.GetComponent<Door>();
        state = DoorBrushState.Brasier;
    }

    public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        var toErase = GetObjectInCell(gridLayout, brushTarget.transform, new Vector3Int(position.x, position.y, zPos));
        if (toErase != null)
            DestroyImmediate(toErase.gameObject);
    }

    public void ResetState()
    {
        state = DoorBrushState.Door;
        previousDoor = null;
    }
    
    private static Transform GetObjectInCell(GridLayout gridLayout, Transform parent, Vector3Int position)
    {
        int childCount = parent.childCount;
        Vector3 min = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(position));
        Vector3 max = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(position + new Vector3Int(1, 1, 0)));
        Bounds bound = new Bounds((max + min) * .5f, max -min );

        for (int i = 0; i < childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (bound.Contains(child.position))
            {    
                return child; 
            }
        }

        return null;
    }
    
}

internal enum DoorBrushState
{
    Door,
    Brasier
}

#endif