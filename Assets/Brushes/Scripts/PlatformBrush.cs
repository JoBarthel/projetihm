﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;

[CreateAssetMenu(order = 2, menuName = "Brushes/PlatformBrush", fileName = "PlatformBrush")]
[CustomGridBrush(false, true, false, "Prefab Brush / Platform Brush")]
public class PlatformBrush : UnityEditor.Tilemaps.GridBrush
{
    [SerializeField] private SpriteRenderer _platform;
    [SerializeField] private int zPos;
    public Sprite currentSprite;
    private float currentRotation;

    public float CurrentRotation => currentRotation;

    public override void Paint(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        Vector3Int cellPosition = new Vector3Int(position.x, position.y, zPos);
        
        if(GetObjectInCell(gridLayout, brushTarget.transform, new Vector3Int(position.x, position.y, zPos)) != null)
            return;
        SpriteRenderer instance = PrefabUtility.InstantiatePrefab(_platform, brushTarget.transform) as SpriteRenderer;
        instance.sprite = currentSprite;
        Vector3 centerTile = new Vector3(.5f, .5f, 0f);
        instance.transform.position =
            gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(cellPosition + centerTile));
        instance.transform.Rotate(-instance.transform.forward, CurrentRotation);

    }

    public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position)
    {
        var toErase = GetObjectInCell(gridLayout, brushTarget.transform, new Vector3Int(position.x, position.y, zPos));
        if (toErase != null)
            DestroyImmediate(toErase.gameObject);
    }

    public void RotateTile()
    {
        currentRotation = CurrentRotation + 90f;
        if (currentRotation >= 360) currentRotation -= 360;
    }
    
    private static Transform GetObjectInCell(GridLayout gridLayout, Transform parent, Vector3Int position)
    {
        int childCount = parent.childCount;
        Vector3 min = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(position));
        Vector3 max = gridLayout.LocalToWorld(gridLayout.CellToLocalInterpolated(position + new Vector3Int(1, 1, 0)));
        Bounds bound = new Bounds((max + min) * .5f, max -min );

        for (int i = 0; i < childCount; i++)
        {
            Transform child = parent.GetChild(i);
            if (bound.Contains(child.position))
            {    
                return child; 
            }
        }

        return null;
    }
}

#endif