﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

[RequireComponent(typeof(Controller))]
[RequireComponent(typeof(AudioSource))]
public class PlayerAvatarAudioSource : MonoBehaviour
{
    [SerializeField] private AudioClip _landingSound;
    [SerializeField] private AudioClip _jumpingSound;
    [SerializeField] private AudioClip _deathSound;
    private Controller _controller;
    private AudioSource _audioSource;

    [SerializeField] private float _randomizePitchAmount;
    private float _basePitch;
    
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _basePitch = _audioSource.pitch;
        _controller = GetComponent<Controller>();
        _controller.Landing += delegate { Play(_landingSound); };
        _controller.Jumping += delegate { Play(_jumpingSound); };
        _controller.Respawning += delegate { Play(_deathSound); };
    }

    void Play(AudioClip sound)
    {
        float extraPitch = UnityEngine.Random.Range(-_randomizePitchAmount, _randomizePitchAmount);
        _audioSource.pitch = _basePitch + extraPitch;
        _audioSource.PlayOneShot(sound);
    }
}
