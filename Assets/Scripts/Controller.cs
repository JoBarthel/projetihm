﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using XInputDotNetPure;

[RequireComponent(typeof(Transform))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(InputManager))]
public class Controller : MonoBehaviour
{
    private struct CurveImpulse
    {
        public readonly AnimationCurve Curve;
        public readonly Vector3 Orientation;
        public float Time;

        public CurveImpulse(AnimationCurve curve, float time, Vector3 orientation)
        {
            Curve = curve;
            Orientation = orientation.normalized;
            Time = time;
        }

        public CurveImpulse(AnimationCurve curve, Vector3 orientation)
        {
            Curve = curve;
            Orientation = orientation.normalized;
            Time = curve.keys[0].time;
        }
    }

    private const int JumpCurveDivision = 50;

    private StatsLoader _stats;
    private Transform _transform;
    private Collider2D _collider;
    private InputManager _inputManager;
    private Animator _animator;
    private static readonly int Jump = Animator.StringToHash("jump");

    private Vector2 _spawnPoint;
    private Vector3 _previousPosition;

    private int _groundCollisionCount;
    private int _wallCollisionCount;

    private bool _isOnGround = true;
    private bool _isOnWall;
    private bool _isJumping;

    private bool _useOverrideDirection;
    private float _overrideDirection;

    private Direction _wallDirection;

    private float _jumpX;
    private float _jumpY;
    private Vector2 _highestJumpPoint;

    private SpriteRenderer _spriteRenderer;

    private List<CurveImpulse> _impulses = new List<CurveImpulse>();

#if UNITY_EDITOR
    public Vector3 Orientation;
    public AnimationCurve Curve;
#endif

    public Vector3 Position => _transform.position;

    public Vector2 SpawnPoint
    {
        get => _spawnPoint;
        set => _spawnPoint = value;
    }

    public delegate void OnRespawn();

    public event OnRespawn Respawning;

    public delegate void OnLanding();
    public event OnLanding Landing;
    private void Awake()
    {
        _transform = GetComponent<Transform>();
        _collider = GetComponentInChildren<Collider2D>();
        _inputManager = GetComponent<InputManager>();
        _animator = GetComponentInChildren<Animator>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if (vfxCollection != null)
            vfxCollection.Init();
    }

    private void Start()
    {
        SpawnPoint = transform.position;
        _stats = GetComponent<StatsLoader>();

        var lastX = _stats.JumpCurve.keys[_stats.JumpCurve.length - 1].time;
        var maxX = 0f;
        var maxY = 0f;
        for (var i = 0; i <= JumpCurveDivision; i++)
        {
            var eval = _stats.JumpCurve.Evaluate(i * lastX / JumpCurveDivision);

            if (eval < maxY) continue;

            maxX = i * lastX / JumpCurveDivision;
            maxY = eval;
        }
        _highestJumpPoint = new Vector2(maxX, maxY);
        
        DontDestroyOnLoad(this);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("KillPlayer"))
        {
            Respawning?.Invoke();
            Respawn();
            return;
        }

        if (!_isOnGround)
            _animator.SetTrigger(Land);

        RepositionAfterCollision(other);
    }

    private void OnCollisionStay2D(Collision2D collision) => RepositionAfterCollision(collision);

    private void RepositionAfterCollision(Collision2D collision)
    {
        if (collision.collider.CompareTag("KillPlayer")) return;

        var oldBounds = new Bounds(_previousPosition + _collider.bounds.extents.y * Vector3.up, _collider.bounds.size);
        var playerExtents = _collider.bounds.extents;
        var otherCenter = collision.collider.transform.position;
        var otherExtents = collision.collider.bounds.extents;

        var direction = CollisionManager.GetCollisionDirection(oldBounds, collision.collider.bounds);

        switch (direction)
        {
            case Direction.Down:
                if (_jumpX < _highestJumpPoint.x)
                {
                    _jumpX = _highestJumpPoint.x;
                    _jumpY = _highestJumpPoint.y;
                }
                _transform.position = _transform.position.x * Vector3.right
                                      + (otherCenter.y - otherExtents.y - 2f * playerExtents.y) * Vector3.up;
                break;

            case Direction.Left:
                _isOnWall = true;
                _wallCollisionCount++;
                _wallDirection = _wallDirection == Direction.Right ? Direction.Up : Direction.Left;
                _transform.position = transform.position.y * Vector3.up
                                      + (otherCenter.x - otherExtents.x - playerExtents.x) * Vector3.right;
                break;

            case Direction.Right:
                _isOnWall = true;
                _wallCollisionCount++;
                _wallDirection = _wallDirection == Direction.Left ? Direction.Up : Direction.Right;
                _transform.position = transform.position.y * Vector3.up
                                      + (otherCenter.x + otherExtents.x + playerExtents.x) * Vector3.right;
                break;

            case Direction.Up:
                _groundCollisionCount++;
                currentNumberOfJumps = 0;

                _transform.position = _transform.position.x * Vector3.right
                                      + (otherCenter.y + otherExtents.y) * Vector3.up;

                if (!_isOnGround)
                {
                    Landing?.Invoke();
                    vfxCollection.PlayLandingVFX(transform.position);
                }

                _isOnGround = true;
                _isJumping = false;
                break;

            default:
                break;
        }
    }

    [SerializeField] private VFXCollection vfxCollection;
    private float currentDirection;
    private int currentNumberOfJumps;
    private float timeSinceLastJump;
    private float lastJumpTime;

    private void FixedUpdate()
    {
        // Registering direction
        if (_inputManager.HorizontalInput != 0)
            currentDirection = Input.GetAxisRaw("Horizontal");

        // Registering jump
        if (_jumpX >= _stats.JumpCurve[_stats.JumpCurve.length - 1].time)
            _isJumping = false;

        if (_inputManager.JumpDown
            && (_isOnGround || _isOnWall || currentNumberOfJumps < _stats.NumberOfJumpsAllowed)
            && timeSinceLastJump > _stats.JumpDelay)
        {
            _isJumping = true;
            _jumpX = 0f;
            _jumpY = 0f;
            timeSinceLastJump = 0f;
            lastJumpTime = Time.time;
            _animator.SetTrigger(Jump);
            vfxCollection.PlayJumpVFX(transform.position);
            Jumping.Invoke();
            if (_isOnWall && !_isOnGround)
            {
                AddImpulse(_stats.WallJumpImpulse,
                    _wallDirection == Direction.Up
                    ? Vector3.up
                    : _wallDirection == Direction.Left
                        ? Vector3.left
                        : Vector3.right);
            }

            if (!_isOnWall) currentNumberOfJumps++;
        }

        var translation = currentDirection * GetCurrentMovementSpeed() * Time.deltaTime * Vector3.right;

        if (_isJumping)
        {
            _jumpX += Time.deltaTime;
            var newJumpY = _stats.JumpCurve.Evaluate(_jumpX);
            translation += (newJumpY - _jumpY) * Vector3.up;
            _jumpY = newJumpY;
        }
        else translation += _stats.FallSpeed * Time.deltaTime * Vector3.up;

        // Registering exterior impulses
        for (var i = _impulses.Count - 1; i >= 0; i--)
        {
            var impulse = _impulses[i];
            translation += (impulse.Curve.Evaluate(impulse.Time + Time.deltaTime) - impulse.Curve.Evaluate(impulse.Time)) * impulse.Orientation;
            impulse.Time += Time.deltaTime;

            if (impulse.Time + Time.deltaTime >= impulse.Curve.keys[impulse.Curve.length - 1].time)
                _impulses.RemoveAt(i);
            else
                _impulses[i] = impulse;
        }

        // Doing the movement
        _previousPosition = _transform.position;
        _transform.Translate(translation);

        _groundCollisionCount = 0;
        _wallCollisionCount = 0;
        _wallDirection = Direction.None;
    }

    private void Update()
    {
        timeSinceLastJump += Time.deltaTime;
    }

    private void LateUpdate()
    {
        if (_isOnGround && _groundCollisionCount <= 0) StartCoroutine(CoyoteCoroutine());

        _isOnGround = _groundCollisionCount > 0;
        _isOnWall = _wallCollisionCount > 0;
    }

    private float _currentSpeed = 0;
    private static readonly int Land = Animator.StringToHash("land");

    float GetCurrentMovementSpeed()
    {
        if (_inputManager.HorizontalInput == 0)
        {
            _currentSpeed -= _stats.Inertia * Time.deltaTime;
            if (_currentSpeed <= 0) _currentSpeed = 0;
            return _currentSpeed;
        }
        _currentSpeed += _stats.HorizontalAcceleration * Time.deltaTime;
        if (_currentSpeed >= _stats.MaxMovementSpeed) _currentSpeed = _stats.MaxMovementSpeed;
        return _currentSpeed;
    }

    private IEnumerator CoyoteCoroutine()
    {
        yield return new WaitForSeconds(_stats.CoyoteTime);
        if (currentNumberOfJumps == 0) currentNumberOfJumps++; // First jump is wasted
        _isOnGround = false;
    }

    private IEnumerator OverrideControlsCoroutine()
    {
        var isRightPressed = _inputManager.RightPressed;
        var isLeftPressed = _inputManager.LeftPressed;

        yield return new WaitUntil(() => _inputManager.LeftPressed != isLeftPressed
            || _inputManager.RightPressed != isRightPressed
            || _isOnGround);

        _useOverrideDirection = false;
        _overrideDirection = 0.0f;
    }

    public void Respawn()
    {
        if(_stats.IsInvincibilityOn) return;
        StartCoroutine(Flicker(.5f));

#if !UNITY_WEBGL
        StartCoroutine(Vibrate(.5f, .5f));
#endif
        transform.position = SpawnPoint;
    }

#if !UNITY_WEBGL
    IEnumerator Vibrate(float duration, float maxVibration)
    {
        while (duration >= 0)
        {
            duration -= Time.deltaTime;
            GamePad.SetVibration(0, maxVibration, maxVibration);
            yield return null;
        }

        GamePad.SetVibration(0, 0, 0);
    }
#endif

    IEnumerator Flicker(float duration)
    {
        var tempColor = _spriteRenderer.color;
        while (duration >= 0)
        {
            tempColor.a = Mathf.Cos((duration * 100) - 0.5f ) * 2;
            _spriteRenderer.color = tempColor;
            duration -= Time.deltaTime;
            yield return null;
        }

        tempColor.a = 1;
        _spriteRenderer.color = tempColor;
    }

    public void AddImpulse(AnimationCurve curve, Vector3 orientation)
    {
        _impulses.Add(new CurveImpulse(curve, orientation));
    }

#if UNITY_EDITOR
    public void AddImpulse() => AddImpulse(Curve, Orientation);
#endif

    public delegate void OnJumping();
    public event OnJumping Jumping;
}
