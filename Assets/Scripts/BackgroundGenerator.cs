﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundGenerator : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private GameObject _isoPlane; // Defines bounds for the level
    
    public float Distance;

    public void CreatePlane()
    {
        var isoTransform = _isoPlane.GetComponent<RectTransform>();
        var isoZ = isoTransform.position.z;
        var cameraTransform = _camera.transform;
        var cameraDistance = Mathf.Abs(cameraTransform.position.z - isoZ);
        var cameraFov = _camera.fieldOfView * Mathf.PI / 180.0f;

        if (Distance <= cameraTransform.position.z || cameraDistance <= 0.1f) return;

        var newPlane = Instantiate(_isoPlane, isoTransform.position + Distance * Vector3.forward, Quaternion.identity, transform);
        newPlane.name = "Plane_" + Distance;

        var seenHeightIso = 2.0f * cameraDistance * Mathf.Tan(cameraFov / 2.0f) * isoTransform.localScale.y;
        var scaleFactor = (cameraDistance + Distance) / cameraDistance;
        var heightMargin = seenHeightIso / scaleFactor;
        var widthMargin = seenHeightIso * _camera.aspect / scaleFactor;

        var newPlaneTransform = newPlane.GetComponent<RectTransform>();
        newPlaneTransform.localScale = isoTransform.localScale * scaleFactor;
        newPlaneTransform.sizeDelta += new Vector2(widthMargin, heightMargin);
    }
}

#endif