﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Controller))]
public class ControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        Controller controller = (Controller) target;

        if (GUILayout.Button("Add impulse"))
        {
            controller.AddImpulse();
        }
    }
}

#endif