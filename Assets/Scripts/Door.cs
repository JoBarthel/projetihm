﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    [SerializeField] private DoorPanel doorUp;
    [SerializeField] private DoorPanel doorDown;
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private float openingAnimationDuration;

    private bool _isOpen;
    public void Open()
    {
        if(_isOpen) return;
        _isOpen = true;
        doorDown.Activate();
        doorUp.Activate();
        StartCoroutine(PlayOpeningAnimation());
    }

    IEnumerator PlayOpeningAnimation()
    {
        float currentTime = 0;
        while (currentTime < openingAnimationDuration)
        {
            var fracTime = currentTime / openingAnimationDuration;
            float yPosition = curve.Evaluate(Mathf.Lerp(0, curve[curve.length - 1].time, fracTime));
            doorUp.Raise(yPosition);
            doorDown.Raise(-yPosition);
            yield return null;
            currentTime += Time.deltaTime;
        }
    }
}
