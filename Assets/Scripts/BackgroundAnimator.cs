﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class BackgroundAnimator : MonoBehaviour
{
    [SerializeField] private Sprite[] _sprites;
    [SerializeField] private float animationDelay;
    private float _currentAnimationDelay;
    private int _currentSpriteIndex;
    private Image _spriteRenderer;
    // Update is called once per frame
    private void Awake()
    {
        _spriteRenderer = GetComponent<Image>();
        _spriteRenderer.sprite = _sprites[0];
        _currentSpriteIndex = 0;
    }
    
    void Update()
    {
        if (_currentAnimationDelay < animationDelay)
        {
            _currentAnimationDelay += Time.deltaTime;
            return;
        }

        _currentAnimationDelay = 0;
        _currentSpriteIndex++;
        if (_currentSpriteIndex >= _sprites.Length)
            _currentSpriteIndex = 0;
        _spriteRenderer.sprite = _sprites[_currentSpriteIndex];
    }
}
