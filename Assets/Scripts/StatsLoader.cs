﻿using System;
using UnityEngine;

public class StatsLoader : MonoBehaviour
{
    [SerializeField] private Stats stats; 
    
    private float _maxMovementSpeed = 1.0f;
    private float _horizontalAcceleration;
    private AnimationCurve _jumpCurve = new AnimationCurve(new Keyframe[5]);
    private float _inertia = 1.0f;
    private float _coyoteTime = 0.1f;
    private int _numberOfJumpsAllowed = 2;
    private float _jumpDelay = .1f;
    private float _fallSpeed;
    private AnimationCurve _wallJumpImpulse = new AnimationCurve(new Keyframe[3]);

    public float MaxMovementSpeed
    {
        get => _maxMovementSpeed;
        set => _maxMovementSpeed = value;
    }

    public float HorizontalAcceleration
    {
        get => _horizontalAcceleration;
        set => _horizontalAcceleration = value;
    }

    public AnimationCurve JumpCurve
    {
        get => _jumpCurve;
        set
        {
            _jumpCurve = value;
        }
    }

    public AnimationCurve WallJumpImpulse
    {
        get => _wallJumpImpulse;
        set { _wallJumpImpulse = value; }
    }

    public float Inertia
    {
        get => _inertia;
        set => _inertia = value;
    }

    public float CoyoteTime
    {
        get => _coyoteTime;
        set => _coyoteTime = value;
    }

    public int NumberOfJumpsAllowed
    {
        get => _numberOfJumpsAllowed;
        set => _numberOfJumpsAllowed = value;
    }

    public float JumpDelay
    {
        get => _jumpDelay;
        set => _jumpDelay = value;
    }

    public float FallSpeed
    {
        get => _fallSpeed;
        set => _fallSpeed = value;
    }

    public bool IsInvincibilityOn { get; set; }

    private void Awake()
    {
        MaxMovementSpeed = stats.MaxMovementSpeed;
        HorizontalAcceleration = stats.HorizontalAcceleration;
        JumpCurve = stats.JumpCurve;
        Inertia = stats.Inertia;
        CoyoteTime = stats.CoyoteTime;
        NumberOfJumpsAllowed = stats.NumberOfJumpsAllowed;
        JumpDelay = stats.JumpDelay;
        FallSpeed = stats.FallSpeed;
        WallJumpImpulse = stats.WallJumpImpulse;
    }
}
