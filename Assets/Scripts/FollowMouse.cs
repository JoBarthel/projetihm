﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMouse : MonoBehaviour
{
    [SerializeField] private Camera _uiCamera;

    private void Update()
    {
        transform.position = _uiCamera.ScreenToWorldPoint(Input.mousePosition);
    }
}
