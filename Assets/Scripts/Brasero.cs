﻿using System;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Brasero : MonoBehaviour
{
    [SerializeField] private Door door;
    [SerializeField] private ParticleSystem _fireLine;
    [SerializeField] private ParticleSystem _flames;


    public delegate void OnFireStart();
    public event OnFireStart FireStarts;
    
    public Door ControlledDoor
    {
        get => door;
        set
        {
            door = value;
            Debug.Log("door " + door.name);
        }
    }

    private void Start()
    {
       _flames.gameObject.SetActive(false);
    }

    private bool _isLit;
    private void OnTriggerEnter2D(Collider2D other)
    {
        _flames.gameObject.SetActive(true);
        if (other.CompareTag("Player") && ! _isLit)
        {
            FireStarts?.Invoke();
            var player = other.GetComponent<Controller>();
            player.SpawnPoint = transform.position;
            _fireLine.Play();
            ControlledDoor.Open();
            _isLit = true;
        }
    }

    public void SetFireLineSpeedAndRotation(Vector3 doorPosition)
    {
        float distance = ( (Vector2) doorPosition - (Vector2)_fireLine.transform.position).magnitude;
        var temp = _fireLine.main;
        temp.startSpeed = distance / _fireLine.main.startLifetimeMultiplier;
        _fireLine.transform.LookAt(doorPosition);
    }
}
