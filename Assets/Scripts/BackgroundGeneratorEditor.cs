﻿#if UNITY_EDITOR

using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(BackgroundGenerator))]
public class BackgroundGeneratorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        BackgroundGenerator bgGenerator = (BackgroundGenerator) target;

        if (GUILayout.Button("Create Plane"))
        {
            bgGenerator.CreatePlane();
        }
    }
}

#endif