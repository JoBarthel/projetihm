﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Schema;
using Microsoft.Win32;
using UnityEngine;
using UnityEngine.UI;

public class EditStats : MonoBehaviour
{
    [SerializeField] private StatsLoader statsLoader;
    [SerializeField] private StatName statToEdit;
    private InputField field;
    private Toggle toggle;
    private bool isInitialized;
    private void Start()
    {
        field = GetComponent<InputField>();
        toggle = GetComponent<Toggle>();
        switch (statToEdit)
        {
            case StatName.INERTIA:
                field.text = statsLoader.Inertia.ToString();
                break;
            case StatName.COYOTE_TIME:
                field.text = statsLoader.CoyoteTime.ToString();
                break;
            case StatName.NUMBER_OF_JUMPS:
                field.text = statsLoader.NumberOfJumpsAllowed.ToString();
                break;
            case StatName.MAX_MOVEMENT_SPEED:
                field.text = statsLoader.MaxMovementSpeed.ToString();
                break;
            case StatName.HORIZONTAL_ACCELERATION:
                field.text = statsLoader.HorizontalAcceleration.ToString();
                break;
            case StatName.JUMP_SPEED:
                field.text = "1";
                previousHorizontalScale = 1;
                break;
            case StatName.JUMP_HEIGHT:
                field.text = "1";
                previousVerticalScale = 1;
                break;
        }
        if(toggle != null)
            toggle.onValueChanged.AddListener(ToggleInvicibility);
        isInitialized = true;

    }

    private float previousHorizontalScale;
    private float previousVerticalScale;
    public void EditStat(String newValue)
    {
        if(!isInitialized) return;
        if (!float.TryParse(newValue, out float number))
            return;

        Debug.Log($"number : {number.ToString()}");
        switch (statToEdit)
        {
            case StatName.INERTIA:
                statsLoader.Inertia = number;
                break;
            case StatName.COYOTE_TIME:
                statsLoader.CoyoteTime = number;
                break;
            case StatName.NUMBER_OF_JUMPS:
                statsLoader.NumberOfJumpsAllowed = (int) Math.Floor(number);
                break;
            case StatName.MAX_MOVEMENT_SPEED:
                statsLoader.MaxMovementSpeed = number;
                break;
            case StatName.HORIZONTAL_ACCELERATION:
                statsLoader.HorizontalAcceleration = number;
                break;
            case StatName.JUMP_SPEED:
                if (number == 0){ 
                    number = 1;
                    field.text = "1";
                }
                AnimationCurve newJumpCurve = statsLoader.JumpCurve;
                ScaleHorizontally(ref newJumpCurve, number/previousHorizontalScale);
                statsLoader.JumpCurve = newJumpCurve;
                previousHorizontalScale = number;
                break;
            case StatName.JUMP_HEIGHT:
                if (number == 0){ 
                    number = 1;
                    field.text = "1";
                }
                AnimationCurve tempJumpCurve = statsLoader.JumpCurve;
                ScaleVertically(ref tempJumpCurve, number/previousVerticalScale);
                statsLoader.JumpCurve = tempJumpCurve;
                previousVerticalScale = number;
                break;
                
                
        }
    }

    public void ToggleInvicibility(Boolean status)
    {
        statsLoader.IsInvincibilityOn = status;
    }
    private static void ScaleHorizontally(ref AnimationCurve curve, float scale)
    {
        Keyframe[] keys = curve.keys;
        for (int i = 0; i < curve.length; i++)
        {
            var key = keys[i];
            key.time *= scale;
            keys[i] = key;
        }

        curve.keys = keys;
    }

    private static void ScaleVertically(ref AnimationCurve curve, float scale)
    {
        Keyframe[] keys = curve.keys;
        for (int i = 0; i < curve.length; i++)
        {
            var key = keys[i];
            key.value *= scale;
            keys[i] = key;
        }

        curve.keys = keys;
    }


}

enum StatName
{
    MAX_MOVEMENT_SPEED,
    HORIZONTAL_ACCELERATION,
    NUMBER_OF_JUMPS,
    INERTIA,
    COYOTE_TIME,
    JUMP_HEIGHT,
    JUMP_SPEED,
    INVINCIBILITY
}
