﻿
using UnityEngine;

[CreateAssetMenu(order = 0, menuName = "VFX/VFXCollection", fileName = "VFXCollection")]
public class VFXCollection : ScriptableObject
{
    public void Init()
    {
        _onJumpInstance = Instantiate(OnJump);
        _onLandingInstance = Instantiate(OnLanding);
    }

    public void PlayJumpVFX(Vector3 position)
    {
        _onJumpInstance.transform.position = position;
        _onJumpInstance.Play();
    }

    public void PlayLandingVFX(Vector3 position)
    {
        _onLandingInstance.transform.position = position;
        _onLandingInstance.Play();
    }
    
    private ParticleSystem _onJumpInstance;
    [SerializeField] private ParticleSystem OnJump;

    private ParticleSystem _onLandingInstance;
    [SerializeField] private ParticleSystem OnLanding;

}
