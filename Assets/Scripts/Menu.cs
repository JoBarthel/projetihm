﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private String _firstLevel;
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _mainMenu;

    public static Menu instance;
    public void Awake()
    {
        if (instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(this);    
    }
    
    public void MainPlay()
    {
        _mainMenu.SetActive(false);
        SceneManager.LoadScene(_firstLevel);
    }

    public void MainSettings()
    {
        _settings.SetActive(true);
    }

    public void MainExit()
    {
        Application.Quit();
    }

    public void SettingsExit()
    {
        _settings.SetActive(false);
        _mainMenu.SetActive(true);
    }
}
