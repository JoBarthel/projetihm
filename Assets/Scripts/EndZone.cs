﻿using System;
using UnityEngine;

public class EndZone : MonoBehaviour
{
    [SerializeField] private FadeInOnVictory winScreen;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
            FadeInOnVictory.instance.StartFadeIn();
    }
}
