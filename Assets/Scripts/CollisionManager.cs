﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    Up = 0,
    Down,
    Left,
    Right,
    None
}

public static class CollisionManager
{
    private const float CollisionMarginLarge = 0.5f;
    private const float CollisionMarginSmall = 0.1f;

    public static Direction GetCollisionDirection(Bounds object1, Bounds object2)
    {
        var direction = Direction.None;

        if (object1.center.y - object1.extents.y > object2.center.y + object2.extents.y - CollisionMarginLarge
            && object1.center.x - object1.extents.x < object2.center.x + object2.extents.x - CollisionMarginSmall
            && object1.center.x + object1.extents.x > object2.center.x - object2.extents.x + CollisionMarginSmall)
            direction = Direction.Up;
        else if (object1.center.y + object1.extents.y < object2.center.y - object2.extents.y + CollisionMarginLarge
            && object1.center.x - object1.extents.x < object2.center.x + object2.extents.x - CollisionMarginSmall
            && object1.center.x + object1.extents.x > object2.center.x - object2.extents.x + CollisionMarginSmall)
            direction = Direction.Down;
        else if (object1.center.x + object1.extents.x < object2.center.x - object2.extents.x + CollisionMarginLarge
            && object1.center.y - object1.extents.y < object2.center.y + object2.extents.y - CollisionMarginSmall
            && object1.center.y + object1.extents.y > object2.center.y - object2.extents.y + CollisionMarginSmall)
            direction = Direction.Left;
        else if (object1.center.x - object1.extents.x > object2.center.x + object2.extents.x - CollisionMarginLarge
            && object1.center.y - object1.extents.y < object2.center.y + object2.extents.y - CollisionMarginSmall
            && object1.center.y + object1.extents.y > object2.center.y - object2.extents.y + CollisionMarginSmall)
            direction = Direction.Right;

        return direction;
    }

    public static Direction GetCollisionDirection(Collider2D object1, Collider2D object2) => GetCollisionDirection(object1.bounds, object2.bounds);

    public static Vector2 GetCollisionVector(Bounds object1, Bounds object2)
    {
        var direction = GetCollisionDirection(object1, object2);

        switch (direction)
        {
            case Direction.Up: return Vector2.up;
            case Direction.Down: return Vector2.down;
            case Direction.Left: return Vector2.left;
            case Direction.Right: return Vector2.right;
            default: return Vector2.zero;
        }
    }

    public static Vector2 GetCollisionVector(Collider2D object1, Collider2D object2) => GetCollisionVector(object1.bounds, object2.bounds);
}
