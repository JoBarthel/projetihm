﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class BouncingBlock : MonoBehaviour
{
    [SerializeField] private AnimationCurve _bounceCurve;

    private Collider2D _collider;
    private Animator _animator;
    private AudioSource _audioSource;
    
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _animator = GetComponent<Animator>();
        _collider = GetComponent<Collider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var controller = collision.collider.GetComponent<Controller>();

        if (controller == null) return;

        var direction = CollisionManager.GetCollisionVector(collision.collider, _collider);

        controller.AddImpulse(_bounceCurve, direction);
        _animator.SetTrigger("bounce");
        _audioSource.Play();
    }
}
