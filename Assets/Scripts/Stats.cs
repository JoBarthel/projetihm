﻿using UnityEngine;

[CreateAssetMenu(order = 1, menuName = "Stats", fileName = "Stats")]
public class Stats : ScriptableObject 
{
    #region Fields

    [Header("Movement")]
    [SerializeField] [Range(0.0f, 20.0f)] [Tooltip("Units travelled per second.")]
    private float _maxMovementSpeed = 1.0f;
    [SerializeField] [Range(0.0f, 200.0f)] [Tooltip("Acceleration in units * s^-2.")]
    private float _horizontalAcceleration;
    [SerializeField] [Range(0.0f, 200.0f)] [Tooltip("Inertia. The higher it is, the more slippery it gets.")]
    private float _inertia = 1.0f;
    [Space]
    [Header("Jump")]
    [SerializeField] [Tooltip("Shape of the jump, x is time in seconds and y is height in units.")]
    private AnimationCurve _jumpCurve = new AnimationCurve(new Keyframe[5]);
    [SerializeField] [Range(0.0f, 0.5f)] [Tooltip("Time after exiting a platform during which the player is still considered on ground.")]
    private float _coyoteTime = 0.1f;
    [SerializeField] [Range(0, 10)] [Tooltip("Number of jumps the player can do.")]
    private int _numberOfJumpsAllowed = 1;
    [SerializeField] [Range(0.0f, 0.5f)] [Tooltip("Minimum delay between two jumps.")]
    private float _jumpDelay = 0.1f;
    [SerializeField] [Tooltip("Horizontal impulse on a wall jump.")]
    private AnimationCurve _wallJumpImpulse;

    #endregion

    #region Properties

    public float MaxMovementSpeed => _maxMovementSpeed;
    public AnimationCurve JumpCurve => _jumpCurve;
    public float Inertia => _inertia;
  
    public float FallSpeed => _jumpCurve[_jumpCurve.length - 1].outTangent;

    public float HorizontalAcceleration => _horizontalAcceleration;

    public float CoyoteTime => _coyoteTime;

    public int NumberOfJumpsAllowed => _numberOfJumpsAllowed;

    public float JumpDelay => _jumpDelay;

    public AnimationCurve WallJumpImpulse => _wallJumpImpulse;

    #endregion
}
