﻿using UnityEngine;

public class DoorPanel : MonoBehaviour
{

    [SerializeField] private GameObject light;

    private void Start()
    {
        light.SetActive(false);
    }

    public void Raise(float yPosition)
    {
        Vector3 newPos = transform.localPosition;
        newPos.y = yPosition;
        transform.localPosition = newPos;
        
    }

    public void Activate()
    {
        light.SetActive(true);
    }
}
