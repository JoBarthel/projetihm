﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private bool _followPlayer;
    private const float Smooth = 0.5f;
    private const float MaxSpeed = 50.0f;

    public GameObject FollowedObject;

    private Transform _transform;
    private Vector3 _smoothVelocity;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void Start()
    {
        _transform.position = FollowedObject.transform.position.x * Vector3.right
            + FollowedObject.transform.position.y * Vector3.up
            + _transform.position.z * Vector3.forward;

        if (_followPlayer) FollowedObject = PlayerSingleton.instance.gameObject;
    }

    private void Update()
    {
        var smoothDamp = Vector3.SmoothDamp(_transform.position, FollowedObject.transform.position,
            ref _smoothVelocity, Smooth, MaxSpeed);
        _transform.position = smoothDamp + (_transform.position.z - smoothDamp.z) * Vector3.forward;
    }
}