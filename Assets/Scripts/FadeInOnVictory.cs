﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CanvasGroup))]
public class FadeInOnVictory : MonoBehaviour
{
    [SerializeField] private AnimationCurve curve;
    [SerializeField] private float animationTime;
    [SerializeField] private String[] scenes;
    [SerializeField] private int nextSceneToLoad;
    [SerializeField] private bool isFirstScene;
    private CanvasGroup _canvas;
    public static FadeInOnVictory instance;
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if(instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        _canvas = GetComponent<CanvasGroup>();
        if (!isFirstScene)
        {
            _canvas.alpha = 1;
            StartCoroutine(Fade(curve[curve.length - 1].time, 0));
        }
    }

    // Update is called once per frame
    public void StartFadeIn()
    {
        StartCoroutine(Fade(  0, curve[curve.length - 1].time, LoadNextScene));
    }

    private void LoadNextScene()
    {
        if(nextSceneToLoad >= scenes.Length) return;
        string nextScene = scenes[nextSceneToLoad];
        if(string.IsNullOrEmpty(nextScene))
            return;
        if (isFirstScene) isFirstScene = false;
        if (Application.CanStreamedLevelBeLoaded(nextScene))
        {
            SceneManager.LoadScene(nextScene);
        }

        StartCoroutine(Fade(1, 0));
        nextSceneToLoad++;

    }
    private IEnumerator Fade(float alphaAtStart, float alphaAtEnd, UnityAction callback = null)
    {
        float currentTime = 0;
        while (currentTime < animationTime)
        {
            var fracTime = currentTime / animationTime;
            var newAlpha = curve.Evaluate(Mathf.Lerp(alphaAtStart, alphaAtEnd, fracTime));
            _canvas.alpha = newAlpha;
            yield return null;
            currentTime += Time.deltaTime;
        }
        
        callback?.Invoke();
    }
}
