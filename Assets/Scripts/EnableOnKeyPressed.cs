﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableOnKeyPressed : MonoBehaviour
{

    [SerializeField] private KeyCode key;

    [SerializeField] private GameObject toEnable;

    private bool isEnabled;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(key))
        {
            if(isEnabled)
                toEnable.SetActive(false);
            else
                toEnable.SetActive(true);
            isEnabled = !isEnabled;
        }


    }
}
