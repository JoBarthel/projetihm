﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    public float HorizontalInput { get; private set; }

    public bool JumpPressed { get; private set; }
    public bool JumpDown { get; private set; }

    public bool RightUp { get; private set; }
    public bool RightDown { get; private set; }
    public bool RightPressed { get; private set; }

    public bool LeftUp { get; private set; }
    public bool LeftDown { get; private set; }
    public bool LeftPressed { get; private set; }

    private bool _oldJumpPressed;

    private void FixedUpdate()
    {
        HorizontalInput = Input.GetAxis("Horizontal");
        var rawHorizontalAxis = Input.GetAxisRaw("Horizontal");
        var jumpButtonPressed = Input.GetButton("Jump");

        if (HorizontalInput * HorizontalInput < 0.01f) HorizontalInput = 0f;

        RightDown = rawHorizontalAxis > 0.5f && !RightPressed;
        RightUp = rawHorizontalAxis < 0.5f && RightPressed;
        RightPressed = rawHorizontalAxis > 0.5f;

        LeftDown = rawHorizontalAxis < -0.5f && !LeftPressed;
        LeftUp = rawHorizontalAxis > -0.5f && LeftPressed;
        LeftPressed = rawHorizontalAxis < -0.5f;

        JumpDown = jumpButtonPressed && !JumpPressed;
        JumpPressed = jumpButtonPressed;

        _oldJumpPressed = JumpPressed;
    }
}