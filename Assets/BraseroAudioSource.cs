﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Brasero))]
[RequireComponent(typeof(AudioSource))]
public class BraseroAudioSource : MonoBehaviour
{
    [SerializeField] private AudioClip _fireStartSound;
    [SerializeField] private AudioClip _fireBurnSound;
    [SerializeField] private AudioClip _openingSound;
    [SerializeField] private AudioSource _openingEffectAudioSource;
    
    private AudioSource _audioSource;
    private Brasero _brasero;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _brasero = GetComponent<Brasero>();
        _brasero.FireStarts += PlayBraseroActivationSound;
    }

    void PlayBraseroActivationSound()
    {
        _audioSource.PlayOneShot(_fireStartSound);
        _openingEffectAudioSource.PlayOneShot(_openingSound);
        _audioSource.loop = true;
        _audioSource.clip = _fireBurnSound;
        _audioSource.PlayDelayed(_fireStartSound.length);
    }
    
    
    
}
