﻿using UnityEngine;

/**
 * Find a the player and set his position and his spawning point here
 */
public class PlayerStartingPoint : MonoBehaviour
{
    void Start()
    {
        PlayerSingleton.instance.transform.position = transform.position;
        var player = PlayerSingleton.instance.GetComponent<Controller>();
        player.SpawnPoint = transform.position;
    }
}
