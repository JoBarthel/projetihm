﻿using UnityEngine;

public class PlayerSingleton : MonoBehaviour
{
    public static PlayerSingleton instance;
    private void Awake()
    {
        Debug.Log("awake player");
        if (instance == null)
            instance = this;
        else if(instance != this)
            Destroy(this.gameObject);
    }
}
